package com.sys.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "temp", schema="amojeweb")
@SequenceGenerator(
        name = "temp_seq_generator",
        sequenceName = "amojeweb.temp_seq",
        initialValue = 100, allocationSize = 1)
public class TempEntity {

	@Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "temp_seq_generator")
	private Long tempId;

	private String temp1;
	private String temp2;
	private String temp3;
	private String temp4;

}
