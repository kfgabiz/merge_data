package com.sys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
//@EnableJpaAuditing
//@MapperScan(value = {"com.sys.mapper.oracle","com.sys.mapper.postgresql"})
//@MapperScan(value = "com.sys.mapper.oracle,com.sys.mapper.postgresql")
//@EnableJpaRepositories(basePackages = {"com.sys.repository"}) // com.my.jpa.repository 하위에 있는 jpaRepository를 상속한 repository scan
//@EntityScan(basePackages = {"com.sys.entity"}) // com.my.jpa.entity 하위에 있는 @Entity 클래스 scan
public class DataTransApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataTransApplication.class, args);
	}



}
