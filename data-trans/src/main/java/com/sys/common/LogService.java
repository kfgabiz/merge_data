package com.sys.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sys.task.domain.MergeDataLogDomain;
import com.sys.task.domain.MergeDataLogErrDomain;
import com.sys.task.pg.LogMapper;

@Service
public class LogService {

	@Autowired private LogMapper log;

	public int logSave(String step, int id, String tableName, String params, int totalCnt, int excuteCnt, int errorCnt, Boolean flag, String errormsg ) {
		MergeDataLogDomain domain = new MergeDataLogDomain();

		if("1".equals(step)) {
			domain.setMergeDataLogId(log.getId());
			domain.setTableName(tableName);
			domain.setParams(params);
			log.insertLog(domain);
			id = domain.getMergeDataLogId();
		} else if("2".equals(step)) {
			domain.setMergeDataLogId(id);
			domain.setStep("2");
			log.updateLog(domain);
		} else if("3".equals(step)) {
			domain.setStep("3");
			domain.setMergeDataLogId(id);
			domain.setTotalCnt(totalCnt);
			domain.setExcuteCnt(excuteCnt);
			domain.setErrorCnt(errorCnt);
			domain.setFlag(flag);
			domain.setErrormsg(errormsg);
			log.updateLog(domain);
		}
		return id;
	}

	public void logErrSave(int id, int cnt, String params, String error) {
		MergeDataLogErrDomain domain = new MergeDataLogErrDomain();
		domain.setMergeDataLogId(id);
		domain.setExcuteCnt(cnt);
		domain.setParams(params);
		domain.setError(error);
		log.insertErrLog(domain);
	}
}
