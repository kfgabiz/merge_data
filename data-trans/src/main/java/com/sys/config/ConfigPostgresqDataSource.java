package com.sys.config;

import java.io.IOException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(value="com.sys.task.pg", sqlSessionFactoryRef="pSqlSessionFactory")
public class ConfigPostgresqDataSource {

	@Bean
	@Primary
	@ConfigurationProperties("spring.datasource.postgresql")
	public DataSource pDatasource() {
		return DataSourceBuilder.create().type(HikariDataSource.class).build();
	}

	@Autowired
	ApplicationContext applicationContext;



    @Bean
	@Primary
    public SqlSessionFactoryBean pSqlSessionFactory() throws IOException {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(pDatasource());
        factoryBean.setMapperLocations(applicationContext.getResources("classpath:/mapper/postgresql/*-sql.xml"));
        factoryBean.setTypeAliasesPackage("com.sys.task.domain");
        factoryBean.setConfiguration(pgMyBatisConfiguration());
        return factoryBean;

    }



    @Bean
	@Primary
    public SqlSessionTemplate pSqlSession(@Qualifier("pSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);

    }

    @Bean(name="pgMyBatisConfiguration")
    public CustomMyBatisConfiguration pgMyBatisConfiguration() {
        return new CustomMyBatisConfiguration();
    }
}


