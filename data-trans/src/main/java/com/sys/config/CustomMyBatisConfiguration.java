package com.sys.config;

import org.apache.ibatis.session.Configuration;

public class CustomMyBatisConfiguration extends Configuration {
    public CustomMyBatisConfiguration() {
        this.setMapUnderscoreToCamelCase(true);
    }
}
