package com.sys.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {
    @Bean
    public RestTemplate customRestTemplate() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(60 * 60 * 1000);
        httpRequestFactory.setConnectTimeout(60 * 60 * 1000);
        httpRequestFactory.setReadTimeout(60 * 60 * 1000);

        return new RestTemplate(httpRequestFactory);
    }
}
