package com.sys.config;

import java.io.IOException;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@MapperScan(value="com.sys.task.oc", sqlSessionFactoryRef="oracleSqlSessionFactory")
public class ConfigOracleDataSource {

	@Bean
	@ConfigurationProperties("spring.datasource.oracle")
	public DataSource oDatasource() {
		return DataSourceBuilder.create().type(HikariDataSource.class).build();
	}

	@Autowired
	ApplicationContext applicationContext;



    @Bean(name="oracleSqlSessionFactory")
    public SqlSessionFactoryBean sqlSessionFactory() throws IOException {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(oDatasource());
        factoryBean.setMapperLocations(applicationContext.getResources("classpath:/mapper/oracle/*-sql.xml"));
        factoryBean.setTypeAliasesPackage("com.sys.task.domain");
        factoryBean.setConfiguration(ocMyBatisConfiguration());
        return factoryBean;

    }



    @Bean(name="oracleSqlSession")
    public SqlSessionTemplate sqlSession(@Qualifier("oracleSqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);

    }

    @Bean(name="ocMyBatisConfiguration")
    public CustomMyBatisConfiguration ocMyBatisConfiguration() {
        return new CustomMyBatisConfiguration();
    }
}


