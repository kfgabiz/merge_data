package com.sys.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sys.entity.TempEntity;

public interface TempRepository extends JpaRepository<TempEntity, Long> {
	TempEntity findByTempId(Long tempId);
}