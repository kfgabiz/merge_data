package com.sys.task;

import com.sys.common.LogService;
import com.sys.task.domain.ItemSellPriceMstCntDomain;
import com.sys.task.domain.ItemSellPriceMstDomain;
import com.sys.task.oc.ItemSellPriceMstCntOcMapper;
import com.sys.task.pg.ItemSellPriceMstCntPgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@RestController
public class ItemSellPriceMstCnt {

	@Autowired private ItemSellPriceMstCntOcMapper oc;
	@Autowired private ItemSellPriceMstCntPgMapper pg;
	@Autowired private LogService log;

	private final String TABLE_NAME = "item_sell_price_mst";

	/**
     * using : http://localhost:8080/itemSellPriceMst?updateDate=20230705
	 * @param param
	 */
	@GetMapping (value="/itemSellPriceMstCnt")
	@ResponseBody
	public void merge(@RequestParam Map param) {
		// FIXME 테스트용. 삭제 해야함.
//		param.put("updateDate", "20230705");

		int logId = log.logSave("1", 0, TABLE_NAME, param.toString(), 0 ,0, 0, null,null); // 최초 로그 저장

		int excuteCnt = 0;
		int errorCnt = 0;
		int totalCnt = 0;

		Boolean flag = true;
		String err = null;
		try {

			/* 오라클 조회 START */
			ItemSellPriceMstCntDomain domainParam = new ItemSellPriceMstCntDomain();
			List<ItemSellPriceMstCntDomain> list = oc.select(domainParam);
			/* 조회 END */

			log.logSave("2", logId, TABLE_NAME, param.toString(), 0 ,0, 0, null,null); // SELECT 시간 저장

			totalCnt = list.size();
			for(ItemSellPriceMstCntDomain domain : list) {
				try {
					int cnt = 0;

					//오라클 조회 count(price_group_cd)
					Integer ocCnt = domain.getPriceGroupCnt();
					//포스트그레 조회 count(price_group_cd)
					Integer pgCnt = pg.chkCnt(domain);

					//count가 같지않다면 삭제 후 저장
					if(ocCnt != pgCnt) {
						//삭제
						pg.deleteData(domain);

						//저장할 데이터 조회
						List<ItemSellPriceMstDomain> insertList = oc.selectInsert(domain);

						for(ItemSellPriceMstDomain insertDomain : insertList) {
							pg.insertData(insertDomain);
						}
					}

				}catch(Exception e) {
					int l = e.getMessage().length();
					int c = (l < 1000) ? l-1 : 1000;
					log.logErrSave(logId, 0, domain.toString(), e.getMessage().substring(0,c));
					errorCnt++;
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			flag = false;
			int l = e.getMessage().length();
			int c = (l < 1000) ? l-1 : 1000;
			err = e.getMessage().substring(0,c);
			e.printStackTrace();
		}finally {
			log.logSave("3", logId, TABLE_NAME, param.toString(), totalCnt, excuteCnt, errorCnt, flag,err); //완료 로그 저장
		}

	}

	private LocalDateTime parseDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDate ld = LocalDate.parse(date, formatter);
		return ld.atStartOfDay();
	}
}
