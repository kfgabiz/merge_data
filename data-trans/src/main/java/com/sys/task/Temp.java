package com.sys.task;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sys.common.LogService;
import com.sys.task.domain.TempDomain;
import com.sys.task.oc.TempOcMapper;
import com.sys.task.pg.TempPgMapper;

@RestController
public class Temp {

	@Autowired private TempOcMapper oc;
	@Autowired private TempPgMapper pg;
	@Autowired private LogService log;

	@GetMapping (value="/temp")
	@ResponseBody
	public void merge(@RequestParam Map param) {
		int logId = log.logSave("1", 0, "carInSeq", param.toString(), 0 ,0, 0, null,null); // 최초 로그 저장

		int excuteCnt = 0;
		int errorCnt = 0;
		int totalCnt = 0;

		Boolean flag = true;
		String err = null;
		try {

			/* 조회 START */
			List<TempDomain> list = oc.selectTempList1();
			/* 조회 END */

			log.logSave("2", logId, "carInSeq", param.toString(), 0 ,0, 0, null,null); // SELECT 시간 저장

			/* DELETE INSERT시 START */

			/* DELETE INSERT시 END */

			totalCnt = list.size();
			for(TempDomain m : list) {
				try {


					int cnt = 0;
					/* 저장 START - Key O */
					//int cnt =  pg.save(m);

					/* 저장 START - Key X */
					Integer c = pg.chkData(m);

					c = (c == null) ? 0 : c;
					if(c == 0) cnt = pg.insertData(m);
					else if(c == 1) cnt = pg.updateData(m);
					else cnt = c;

					/* 저장 END */
					if(cnt > 1) {
						log.logErrSave(logId, cnt, m.toString(),null);
						errorCnt++;
					}else {
						excuteCnt++;
					}
				}catch(Exception e) {
					int l = e.getMessage().length();
					int c = (l < 1000) ? l-1 : 1000;
					log.logErrSave(logId, 0, m.toString(), e.getMessage().substring(0,c));
					errorCnt++;
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			flag = false;
			int l = e.getMessage().length();
			int c = (l < 1000) ? l-1 : 1000;
			err = e.getMessage().substring(0,c);
			e.printStackTrace();
		}finally {
			log.logSave("3", logId, "carInSeq", param.toString(), totalCnt, excuteCnt, errorCnt, flag,err); //완료 로그 저장
		}

	}
}
