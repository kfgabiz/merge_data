package com.sys.task.pg;

import com.sys.task.domain.DayOrderDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderPgMapper {
//	public List<DayOrderDomain> selectTempList2() ;

//	public int save(DayOrderDomain m);

	public Integer chkData(DayOrderDomain m);

	public int insertData(DayOrderDomain m);

	public int updateData(DayOrderDomain m);

}
