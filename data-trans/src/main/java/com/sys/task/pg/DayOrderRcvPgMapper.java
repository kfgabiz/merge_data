package com.sys.task.pg;

import com.sys.task.domain.DayOrderRcvDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderRcvPgMapper {
//	public List<DayOrderDomain> selectTempList2() ;

//	public int save(DayOrderDomain m);

	public Integer chkData(DayOrderRcvDomain m);

	public int insertData(DayOrderRcvDomain m);

	public int updateData(DayOrderRcvDomain m);

}
