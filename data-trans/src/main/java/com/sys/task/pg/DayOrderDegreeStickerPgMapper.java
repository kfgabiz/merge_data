package com.sys.task.pg;

import com.sys.task.domain.DayOrderDegreeStickerDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderDegreeStickerPgMapper {
//	public List<DayOrderDomain> selectTempList2() ;

//	public int save(DayOrderDomain m);

	public Integer chkData(DayOrderDegreeStickerDomain m);

	public int insertData(DayOrderDegreeStickerDomain m);

	public int updateData(DayOrderDegreeStickerDomain m);

}
