package com.sys.task.pg;

import com.sys.task.domain.OutInfoDomain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OutInfoPgMapper {
//	List<OutInfoDomain> selectTempList2() ;

//	int save(OutInfoDomain m);

	Integer chkData(OutInfoDomain m);

	int insertData(OutInfoDomain m);

	int updateData(OutInfoDomain m);

}
