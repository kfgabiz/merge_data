package com.sys.task.pg;

import com.sys.task.domain.ItemSellPriceMstCntDomain;
import com.sys.task.domain.ItemSellPriceMstDomain;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ItemSellPriceMstCntPgMapper {
//	List<ItemSellPriceMstDomain> selectTempList2() ;

//	int save(ItemSellPriceMstDomain m);

	Integer chkCnt(ItemSellPriceMstCntDomain m);

	int insertData(ItemSellPriceMstDomain m);

	int deleteData(ItemSellPriceMstCntDomain m);

}
