package com.sys.task.pg;

import com.sys.task.domain.DayOrderHistDomain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DayOrderHistPgMapper {
//	public List<DayOrderHistDomain> selectTempList2() ;

//	public int save(DayOrderHistDomain m);

	public Integer chkData(DayOrderHistDomain m);

	public int insertData(DayOrderHistDomain m);

	public int updateData(DayOrderHistDomain m);

}
