package com.sys.task.pg;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sys.task.domain.TempDomain;

@Mapper
public interface TempPgMapper {
	public List<TempDomain> selectTempList2() ;

	public int save(TempDomain m);

	public Integer chkData(TempDomain m);

	public int insertData(TempDomain m);

	public int updateData(TempDomain m);

}
