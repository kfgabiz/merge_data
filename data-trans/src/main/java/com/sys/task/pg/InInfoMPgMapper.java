package com.sys.task.pg;

import com.sys.task.domain.InInfoMDomain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InInfoMPgMapper {

	Integer chkData(InInfoMDomain m);

	int insertData(InInfoMDomain m);

	int updateData(InInfoMDomain m);

}
