package com.sys.task.pg;

import com.sys.task.domain.DayOrderDegreeDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderDegreePgMapper {
//	public List<DayOrderDomain> selectTempList2() ;

//	public int save(DayOrderDomain m);

	public Integer chkData(DayOrderDegreeDomain m);

	public int insertData(DayOrderDegreeDomain m);

	public int updateData(DayOrderDegreeDomain m);

}
