package com.sys.task.pg;

import com.sys.task.domain.InInfoDomain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InInfoPgMapper {

	Integer chkData(InInfoDomain m);

	int insertData(InInfoDomain m);

	int updateData(InInfoDomain m);

}
