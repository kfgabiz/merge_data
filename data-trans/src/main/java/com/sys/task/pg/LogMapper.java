package com.sys.task.pg;

import org.apache.ibatis.annotations.Mapper;

import com.sys.task.domain.MergeDataLogDomain;
import com.sys.task.domain.MergeDataLogErrDomain;

@Mapper
public interface LogMapper {

	Integer getId();

	void insertLog(MergeDataLogDomain domain);

	void updateLog(MergeDataLogDomain domain);

	void insertErrLog(MergeDataLogErrDomain domain);

}
