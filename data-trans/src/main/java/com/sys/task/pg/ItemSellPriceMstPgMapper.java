package com.sys.task.pg;

import com.sys.task.domain.ItemSellPriceMstDomain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ItemSellPriceMstPgMapper {
//	List<ItemSellPriceMstDomain> selectTempList2() ;

//	int save(ItemSellPriceMstDomain m);

	Integer chkData(ItemSellPriceMstDomain m);

	int insertData(ItemSellPriceMstDomain m);

	int updateData(ItemSellPriceMstDomain m);

}
