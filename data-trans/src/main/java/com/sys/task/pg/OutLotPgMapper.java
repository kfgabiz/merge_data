package com.sys.task.pg;

import com.sys.task.domain.OutLotDomain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OutLotPgMapper {
//	List<OutLotDomain> selectTempList2() ;

//	int save(OutLotDomain m);

	Integer chkData(OutLotDomain m);

	int insertData(OutLotDomain m);

	int updateData(OutLotDomain m);

}
