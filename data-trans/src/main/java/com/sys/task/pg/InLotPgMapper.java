package com.sys.task.pg;

import com.sys.task.domain.InLotDomain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InLotPgMapper {
//	List<OutLotDomain> selectTempList2() ;

//	int save(OutLotDomain m);

	Integer chkData(InLotDomain m);

	int insertData(InLotDomain m);

	int updateData(InLotDomain m);

}
