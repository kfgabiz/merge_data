package com.sys.task.pg;

import com.sys.task.domain.InoutYmdDomain;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InoutYmdPgMapper {
//	public List<InoutYmdDomain> selectTempList2() ;

//	public int save(InoutYmdDomain m);

	Integer chkData(InoutYmdDomain m);

	int insertData(InoutYmdDomain m);

	int updateData(InoutYmdDomain m);

}
