package com.sys.task.pg;

import com.sys.task.domain.DayOrderClosingDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderClosingPgMapper {
//	public List<DayOrderClosingDomain> selectTempList2() ;

//	public int save(DayOrderClosingDomain m);

	public Integer chkData(DayOrderClosingDomain m);

	public int insertData(DayOrderClosingDomain m);

	public int updateData(DayOrderClosingDomain m);

}
