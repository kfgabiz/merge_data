package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("dayOrderDegreeStickerDomain")
public class DayOrderDegreeStickerDomain extends BaseDomain {

	private String rowid;

	/* keys */
	private Double sequenceId;
	private String orderNo;
	private Double seq;
	private String itemCd;
	private String itemName;
	private String estCd;
	private String estName;
	private String stockDate;

	private String sellerCd;
	private String sellerName;
	private String orderUnit;
	private Double orderQty;
	private Double packingType;
	private Double boxQty;
	private Double lastQty;
	private Double labelQty;
	private String confirmCarSeq;
	private String remarks;
	private String productOrigin;
	private String attribute01;
	private String attribute02;
	private String attribute03;
	private String attribute04;
	private String attribute05;
	private String attribute06;
	private String attribute07;
	private String attribute08;
	private String attribute09;
	private String attribute10;
	private String buyRateGbn;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;
}
