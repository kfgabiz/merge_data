package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("dayOrderRcvDomain")
public class DayOrderRcvDomain extends BaseDomain {

	private String rowid;

	/* keys */
	private String orderNo;
	private Double seq;
	private String itemCd;
	private String estCd;
	private String stockDate;
	private Double rcvNo;

	private String sellerCd;
	private String logCd;
	private String deliveGbn;
	private String itemGbn;
	private String itemName;
	private String spec;
	private String stockUnit;
	private String saleUnit;
	private String buyRate;
	private Double trnsRate;
	private String orderGbn;
	private String trxGbn;
	private Double confirmOrderQty;
	private Double orderQty;
	private Double innerPrice;
	private Double sellPrice;
	private Double taxAmt;
	private String carSeq;
	private String confirmCarSeq;
	private String orderConfirmYn;
	private String stockType;
	private LocalDateTime rcvDate;
	private String baecha;
	private String ordClosingTime;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;
}
