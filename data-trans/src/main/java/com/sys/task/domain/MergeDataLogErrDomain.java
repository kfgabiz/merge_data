package com.sys.task.domain;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("logerr")
public class MergeDataLogErrDomain {
	private int mergeDataLogId ;
	private String params;
	private String error;
	private int excuteCnt;
}
