package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("outInfoDomain")
public class OutInfoDomain extends BaseDomain {

	private String rowid;

	/* keys */
	private String logCd;
	private String outDate;
	private Double outNo;
	private Double outSeq;

	private String outTypeCode;
	private String outWhCode;
	private String compCd;
	private Double orderQty;
	private String itemGbn;
	private String deliveGbn;
	private String etcOutReasonCode;
	private Double outQty;
	private Double salePrice;
	private Double saleAmt;
	private Double taxAmt;
	private String taxType;
	private String orderNo;
	private Double seq;
	private String itemCd;
	private String itemName;
	private String spec;
	private Double trnsRate;
	private String stockUnit;
	private String saleUnit;
	private String estCd;
	private String dlvyEstCd;
	private String orderGbn;
	private String stockDate;
	private String remark;
	private String notOutReasonCode;
	private String csConfirmCode;
	private String csConfirmRemark;
	private String delYn;
	private String outConfirmYn;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;
	private String inoutYn;
	private String applDate;
	private String salesDt;
	private String coaAccount;
	private String stockInNo;
	private String etcOutReasonRemark;
	private String notOutGbn;
	private String csConfirmUserId;
	private LocalDateTime csConfirmDate;
	private String mdUpdateUserId;
	private LocalDateTime mdUpdateDate;
	private String loUpdateUserId;
	private LocalDateTime loUpdateDate;
	private String loConfirmCode;
	private String imCode;
	private String mdConfirmCode;
	private String mdConfirmRemark;
	private String loConfirmRemark;
	private Double returnOutSeq;

}
