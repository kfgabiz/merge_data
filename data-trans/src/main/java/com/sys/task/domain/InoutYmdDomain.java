package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("inoutYmdDomain")
public class InoutYmdDomain extends BaseDomain {

	private String rowid;

	/* keys */
	private String logCd;
	private String itemCd;
	private String inoutYm;
	private String whCode;
	private String inoutYmd;

	private String compCd;
	private Double befCloseQty;
	private Double befCloseAmt;
	private Double inoutQty1;
	private Double inoutQty2;
	private Double inoutQty3;
	private Double inoutQty4;
	private Double inoutQty5;
	private Double inoutQty6;
	private Double inoutQty7;
	private Double inoutQty8;
	private Double inoutQty9;
	private Double inoutQty10;
	private Double inoutQty11;
	private Double inoutQty12;
	private Double inoutQty13;
	private Double inoutQty14;
	private Double inoutQty15;
	private Double inoutAmt2;
	private Double inoutAmt4;
	private Double inoutQty16;
	private Double inoutAmt16;
	private Double inoutAmt11;
	private Double inoutAmt12;
	private Double inoutAmt13;
	private Double inoutQty17;
	private Double inoutQty18;
}
