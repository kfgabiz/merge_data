package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("inLotDomain")
public class InLotDomain extends BaseDomain {

	private String rowid;

	/* keys */
	private String logCd;
	private String inDate;
	private Double inNo;
	private Double inSeq;
	private String distDate;

	private Double inQty;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;

}
