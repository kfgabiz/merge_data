package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("inInfoDomain")
public class InInfoDomain extends BaseDomain {

	private String rowid;

	/* keys */
	private String logCd;
	private String inDate;
	private Double inNo;
	private Double inSeq;

	private String inTypeCode;
	private String compCd;
	private String inWhCode;
	private String sellerCd;
	private String itemGbn;
	private String deliveGbn;
	private String orderNo;
	private Double seq;
	private String itemCd;
	private String itemName;
	private String spec;
	private String stockDate;
	private String estCd;
	private Double inReqQty;
	private Double inQty;
	private String stockUnit;
	private String orderUnit;
	private String buyRate;
	private Double inPrice;
	private Double inAmt;
	private Double taxAmt;
	private String taxType;
	private String notInReason;
	private String inConfirmYn;
	private String delYn;
	private String remark;
	private String etcInReasonCode;
	private String saleUnit;
	private Double trnsRate;
	private String stockType;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;
	
	
}
