package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("itemSellPriceMstDomain")
public class ItemSellPriceMstDomain extends BaseDomain {

	private String rowid;

	/* keys */
	// nothing

	/* temp keys */
	private String priceGroupCd;
	private String itemCd;
	private String startDate;
	private String endDate;
	private Double buyPrice;
	private Double innerPrice;
	private Double exRate;
	private Double exPrice;
	private Double dcRate;
	private Double dcPrice;
	private Double deliveryPrice;
	private Double sellPrice;
	private String makeYn;
	private String exYn;
	private String calStd;
	private String remarks;
	private Double standardPrice;

	private String attribute01;
	private String attribute02;
	private String attribute03;
	private String attribute04;
	private String attribute05;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;
}
