package com.sys.task.domain;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("log")
public class MergeDataLogDomain {

	private int mergeDataLogId ;
	private String tableName;
	private String params;
	private String startTime ;
	private int totalCnt;
	private int excuteCnt;
	private int errorCnt;
	private String endTime;
	private Boolean flag;
	private String errormsg;

	private String step;
}
