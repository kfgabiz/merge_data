package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("outLotDomain")
public class OutLotDomain extends BaseDomain {

	private String rowid;

	/* keys */
	private String logCd;
	private String outDate;
	private Double outNo;
	private Double outSeq;
	private String distDate;

	private Double outQty;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;

}
