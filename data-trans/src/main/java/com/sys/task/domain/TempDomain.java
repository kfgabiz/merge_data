package com.sys.task.domain;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("temp")
public class TempDomain extends BaseDomain {

	private Long tempId;
	private String temp1;
	private String temp2;
	private String temp3;
	private String temp4;
}
