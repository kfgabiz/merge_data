package com.sys.task.domain;

import lombok.Data;
import org.apache.ibatis.type.Alias;

import java.time.LocalDateTime;

@Data
@Alias("dayOrderDegreeDomain")
public class DayOrderDegreeDomain extends BaseDomain {

	private String rowid;

	/* keys */
	private String orderNo;
	private Double seq;
	private String itemCd;
	private String estCd;
	private String stockDate;

	private String sellerCd;
	private String logCd;
	private Double orderQty01;
	private Double orderQty02;
	private Double orderQty03;
	private Double orderQty04;
	private Double orderQty05;
	private Double orderQty06;
	private Double orderQty07;
	private Double orderQty08;
	private Double orderQty09;
	private Double orderQty10;
	private String carSeq01;
	private String carSeq02;
	private String carSeq03;
	private String carSeq04;
	private String carSeq05;
	private String carSeq06;
	private String carSeq07;
	private String carSeq08;
	private String carSeq09;
	private String carSeq10;
	private String attribute01;
	private String attribute02;
	private String attribute03;
	private String attribute04;
	private String attribute05;
	private String attribute06;
	private String attribute07;
	private String attribute08;
	private String attribute09;
	private String attribute10;
	private String deliveGbn;
	private String itemGbn;
	private String stockType;
	private String saleUnit;
	private String itemName;
	private String spec;
	private String confirmCarSeq;
	private String remarks;
	private String makeDate;
	private LocalDateTime printDate;
	private String createUserId;
	private LocalDateTime createDate;
	private String updateUserId;
	private LocalDateTime updateDate;

	private int startNum;
	private int endNum;
}
