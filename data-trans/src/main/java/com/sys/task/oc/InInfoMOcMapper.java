package com.sys.task.oc;

import com.sys.task.domain.InInfoMDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InInfoMOcMapper {
	List<InInfoMDomain> select(InInfoMDomain domainParam) ;
}
