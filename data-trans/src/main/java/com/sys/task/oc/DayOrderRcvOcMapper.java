package com.sys.task.oc;

import com.sys.task.domain.DayOrderRcvDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderRcvOcMapper {
	public List<DayOrderRcvDomain> select(DayOrderRcvDomain domainParam) ;
}
