package com.sys.task.oc;

import com.sys.task.domain.DayOrderDegreeStickerDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderDegreeStickerOcMapper {
	public List<DayOrderDegreeStickerDomain> select(DayOrderDegreeStickerDomain domainParam) ;
}
