package com.sys.task.oc;

import com.sys.task.domain.ItemSellPriceMstCntDomain;
import com.sys.task.domain.ItemSellPriceMstDomain;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ItemSellPriceMstCntOcMapper {
	List<ItemSellPriceMstCntDomain> select(ItemSellPriceMstCntDomain domainParam) ;
	List<ItemSellPriceMstDomain> selectInsert(ItemSellPriceMstCntDomain domainParam) ;
}
