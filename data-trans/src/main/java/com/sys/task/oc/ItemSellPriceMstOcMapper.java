package com.sys.task.oc;

import com.sys.task.domain.ItemSellPriceMstDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ItemSellPriceMstOcMapper {
	List<ItemSellPriceMstDomain> select(ItemSellPriceMstDomain domainParam) ;
}
