package com.sys.task.oc;

import com.sys.task.domain.DayOrderClosingDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderClosingOcMapper {
	public List<DayOrderClosingDomain> select(DayOrderClosingDomain domainParam) ;
}
