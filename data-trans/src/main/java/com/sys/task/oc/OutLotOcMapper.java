package com.sys.task.oc;

import com.sys.task.domain.OutLotDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OutLotOcMapper {
	List<OutLotDomain> select(OutLotDomain domainParam) ;
}
