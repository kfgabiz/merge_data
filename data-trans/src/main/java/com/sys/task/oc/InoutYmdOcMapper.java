package com.sys.task.oc;

import com.sys.task.domain.InoutYmdDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InoutYmdOcMapper {
	List<InoutYmdDomain> select() ;

}
