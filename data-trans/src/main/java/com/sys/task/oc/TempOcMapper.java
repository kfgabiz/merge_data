package com.sys.task.oc;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.sys.task.domain.TempDomain;

@Mapper
public interface TempOcMapper {
	public List<TempDomain> selectTempList1() ;
}
