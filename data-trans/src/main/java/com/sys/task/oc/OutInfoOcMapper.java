package com.sys.task.oc;

import com.sys.task.domain.OutInfoDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OutInfoOcMapper {
	List<OutInfoDomain> select(OutInfoDomain domainParam) ;
}
