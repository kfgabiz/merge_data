package com.sys.task.oc;

import com.sys.task.domain.InLotDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InLotOcMapper {
	List<InLotDomain> select(InLotDomain domainParam) ;
}
