package com.sys.task.oc;

import com.sys.task.domain.DayOrderDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderOcMapper {
	public List<DayOrderDomain> select(DayOrderDomain domainParam) ;
}
