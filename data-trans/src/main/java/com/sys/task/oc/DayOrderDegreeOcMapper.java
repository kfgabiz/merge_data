package com.sys.task.oc;

import com.sys.task.domain.DayOrderDegreeDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderDegreeOcMapper {
	public List<DayOrderDegreeDomain> select(DayOrderDegreeDomain domainParam) ;
	public List<DayOrderDegreeDomain> select1(DayOrderDegreeDomain domainParam) ;
}
