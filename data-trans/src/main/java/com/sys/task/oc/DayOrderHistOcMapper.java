package com.sys.task.oc;

import com.sys.task.domain.DayOrderHistDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface DayOrderHistOcMapper {
	public List<DayOrderHistDomain> select(DayOrderHistDomain domainParam) ;
}
