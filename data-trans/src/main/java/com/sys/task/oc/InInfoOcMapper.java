package com.sys.task.oc;

import com.sys.task.domain.InInfoDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface InInfoOcMapper {
	List<InInfoDomain> select(InInfoDomain domainParam) ;
}
