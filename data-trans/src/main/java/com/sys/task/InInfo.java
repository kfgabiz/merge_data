package com.sys.task;

import com.sys.common.LogService;
import com.sys.task.domain.InInfoDomain;
import com.sys.task.oc.InInfoOcMapper;
import com.sys.task.pg.InInfoPgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@RestController
public class InInfo {

	@Autowired private InInfoOcMapper oc;
	@Autowired private InInfoPgMapper pg;
	@Autowired private LogService log;

	private final String TABLE_NAME = "in_info";

	/**
     * using : http://localhost:8080/outInfo?updateDate=20230705
	 * @param param
	 */
	@GetMapping (value="/inInfo")
	@ResponseBody
	public void merge(@RequestParam Map param) {
		// FIXME 테스트용. 삭제 해야함.
//		param.put("updateDate", "20230705");

		int logId = log.logSave("1", 0, TABLE_NAME, param.toString(), 0 ,0, 0, null,null); // 최초 로그 저장

		int excuteCnt = 0;
		int errorCnt = 0;
		int totalCnt = 0;

		Boolean flag = true;
		String err = null;
		try {

			/* 조회 START */
			InInfoDomain domainParam = new InInfoDomain();
			domainParam.setUpdateDate(this.parseDate(param.get("updateDate").toString()));
			List<InInfoDomain> list = oc.select(domainParam);
			/* 조회 END */

			log.logSave("2", logId, TABLE_NAME, param.toString(), 0 ,0, 0, null,null); // SELECT 시간 저장

			/* DELETE INSERT시 START */

			/* DELETE INSERT시 END */

			totalCnt = list.size();
			for(InInfoDomain domain : list) {
				try {
					int cnt = 0;
					/* 저장 START - Key O */
					//int cnt =  pg.save(m);

					/* 저장 START - Key X */
					Integer c = pg.chkData(domain);

					c = (c == null) ? 0 : c;
					if(c == 0) cnt = pg.insertData(domain);
					else if(c == 1) cnt = pg.updateData(domain);
					else cnt = c;

					/* 저장 END */
					if(cnt > 1) {
						log.logErrSave(logId, cnt, domain.toString(),null);
						errorCnt++;
					}else {
						excuteCnt++;
					}
				}catch(Exception e) {
					int l = e.getMessage().length();
					int c = (l < 1000) ? l-1 : 1000;
					log.logErrSave(logId, 0, domain.toString(), e.getMessage().substring(0,c));
					errorCnt++;
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			flag = false;
			int l = e.getMessage().length();
			int c = (l < 1000) ? l-1 : 1000;
			err = e.getMessage().substring(0,c);
			e.printStackTrace();
		}finally {
			log.logSave("3", logId, TABLE_NAME, param.toString(), totalCnt, excuteCnt, errorCnt, flag,err); //완료 로그 저장
		}

	}

	private LocalDateTime parseDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDate ld = LocalDate.parse(date, formatter);
		return ld.atStartOfDay();
	}
}
